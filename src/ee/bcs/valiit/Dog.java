package ee.bcs.valiit;

public class Dog {

    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void bark() {
        System.out.println(name + " tegi auh-auh!");
    }
}
